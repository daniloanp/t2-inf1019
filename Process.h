//
// Created by danilo on 15/11/15.
//

#ifndef T2_INF1019_PROCESS_H
#define T2_INF1019_PROCESS_H

#include <stdbool.h>
#include "Memory.h"


typedef struct _process* Process;

typedef enum
{
    NullPointer = 0,
    Running   = 1,
    WaitingIO = 2,
    Ready     = 3,
    Finished  = 4,
}                      ProcessState;

typedef enum
{
    CPU = 0,
    IO  = 1,
}                      ProcessOperation;

Process         Process_New (int id, int memUsage, int numberOfOperations);

void            Process_Destroy (Process process);

ProcessState    Process_GetState (Process process);

void            Process_AddOperation (Process process, ProcessOperation op, int time);

void            Process_Timer (Process process);

bool Process_Start (Process p);

void            Process_Stop (Process p);

int             Process_GetTimeStopped (Process p);

bool Process_SetMemory (Process process, Memory memory, MemoryPartition (  * memPolicy) (Memory, long));

bool Process_IsInMemory (Process p);

MemoryPartition Process_GetMemoryPartition (Process process);

long Process_GetMemoryUsage (Process processs);

void Process_SetMemoryPartition (Process p, MemoryPartition partition);


int Process_GetID(Process process);

void Process_PrintLastOperation(Process process);

#endif //T2_INF1019_PROCESS_H
