//
// Created by danilo on 15/11/15.
//

#ifndef T2_INF1019_MEMORY_H
#define T2_INF1019_MEMORY_H


#include <stdbool.h>
#include <stdio.h>
#include "List.h"

typedef struct _mem* Memory;
typedef struct _partition* MemoryPartition;
//typedef struct _memInfo* MemoryInfo; // nos dá slot inicial e ponteiro para partição;


//typedef enum {
//    Part1 = 0,
//    Part2 = 0,
//    Part3 = 0,
//    Part4 = 0,
//    Part5 = 0,
//} Partition ;

Memory Memory_New(void);
void Memory_Destroy(Memory memory);



// case alguém falhe, resultado nulo, swap necessário!
MemoryPartition Memory_SwapIn_BestFit (Memory memory, long requiredMemory);
MemoryPartition Memory_SwapIn_WorstFit (Memory memory, long requiredMemory);
MemoryPartition Memory_SwapIn_NextFit (Memory memory, long requiredMemory);
MemoryPartition Memory_SwapIn_FirstFit (Memory memory, long requiredMemory);

long MemoryPartition_GetUsedMemory(MemoryPartition memPartition);
long MemoryPartition_GetLength(MemoryPartition memoryPartition);
void MemoryPartition_SwapOut (MemoryPartition partition);


//void MemoryInfo_SwapOut(Memory memory, MemoryInfo memoryInfo);


//void MemoryInfo_SetProcessID (MemoryInfo memoryPartition, int processID);

void MemoryPartition_SetProcessID (MemoryPartition partition, int processID);

//bool Memory_Contains(Memory memory, MemoryInfo memoryInfo);


void Memory_PrettyPrinter(Memory memory, FILE* file);


#endif //T2_INF1019_MEMORY_H
