#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Process.h"
//

int  maxTime = 0;

//
void readInput (FILE* input, List processList);

void startExec (List processList, MemoryPartition (* memPolicy) (Memory, long));


int readline (FILE* f, char* buffer, size_t len);


char _buffer[256];

void execForPolicy (const char* filepath, MemoryPartition (* memPolicy) (Memory, long))
{
    List processList;
    FILE* input = fopen(filepath, "r");
    if (input == NULL) {
        perror("Não pode ler arquivo!\n");
        exit(1);
    }
    //Best Fit
    processList = List_New((void (*) (void*)) Process_Destroy);
    readInput(input, processList);
    fclose(input);


    startExec(processList, memPolicy);
    List_Destroy(processList);

}

int main (int arc, char** argv)
{
    if (arc != 2) {
        return 1;
    }
    char* inputFilePath = argv[1];

    printf("\n\n");
    printf("\n========= Best-fit! =============\n");
    execForPolicy(inputFilePath, Memory_SwapIn_BestFit);
    printf("\n========= First-fit! =============\n");
    execForPolicy(inputFilePath, Memory_SwapIn_FirstFit);
    printf("\n========= Next-fit! =============\n");
    execForPolicy(inputFilePath, Memory_SwapIn_NextFit);
    printf("\n========= Worst-fit! =============\n");
    execForPolicy(inputFilePath, Memory_SwapIn_WorstFit);
    printf("\n\n");

    return 0;
}


Process getNextReadyProcess (List processList, bool inMemory)
{
    int stoppedTime = -1;
    List_GoFirst(processList);
    Process retProccess = NULL;
    if (List_IsEmpty(processList)) {
        return NULL;
    }
    do {
        Process      process = List_GetCurrent(processList);
        ProcessState state   = Process_GetState(process);
        if (state == Finished) { // IO é ultima instrução!

            List_DeleteCurrent(processList);// correção simples!
            continue;
        }
        if (inMemory && state == Running) {
            return process;
        }
        if (inMemory && !Process_IsInMemory(process)) { // tem que estar na memória se for o caso.
            continue;
        }

        if (state != Ready) { // é pra considerar apenas os prontos
            continue;
        }
        int stp = Process_GetTimeStopped(process);

        if (stp > stoppedTime) {
            retProccess = process;
            stoppedTime = stp;
        }

    } while (List_GoNext(processList));

    return retProccess;
}


bool swapOut (List processList, Memory memory, long requiredMemory)
{
    List memInfoToSwappOut = List_New(NULL); // nada delete
    List_GoFirst(processList);
    do {
        Process process = List_GetCurrent(processList);
        if (Process_IsInMemory(process) && Process_GetState(process) == WaitingIO) {
            MemoryPartition partition = Process_GetMemoryPartition(process);
            if (MemoryPartition_GetLength(partition) >= requiredMemory) { // ok serve!
                MemoryPartition_SwapOut(partition);
                Process_SetMemoryPartition(process, NULL); // processo sai da memoria
                return true;
            }
            // se tá na memória e está em IO, pode sair!
            List_Append(memInfoToSwappOut, Process_GetMemoryPartition(process));
        }
    } while (List_GoNext(processList));

    List_Destroy(memInfoToSwappOut);
    return false;

//    // Argumentos: Memória, MemoriaDosprocessosQuePodemSair, totalNecessário!
//    bool ret = Memory_SwapOut(memory, memInfoToSwappOut, requiredMemory);
//
//
//    if (ret) { // ok, conseguimos espaço contíguo!
//        List_GoFirst(processList);
//        do {
//            Process process = List_GetCurrent(processList);
//            if (Process_IsInMemory(process) && Process_GetState(process) == WaitingIO) {
//                if (!Memory_Contains(memory, Process_GetMemoryPartition(process))) {
//                    Process_SetMemoryPartition(process, NULL); // Não tem mais memoria, fou swappedOut!
//                }
//            }
//        } while (List_GoNext(processList));
//    }
//    List_Destroy(memInfoToSwappOut);
    //return ret;
}

Process chooseNext (List processList, Process current, Memory memory, MemoryPartition (* memPolicy) (Memory, long))
{
    Process candidateToCurrentProcess;
    bool    memChanged = false;
    bool    wasInMem   = false;
    candidateToCurrentProcess = getNextReadyProcess(processList, false);
    //printf("NextOne = %p\n", candidateToCurrentProcess);

    if (candidateToCurrentProcess == NULL) { // Não tem um pŕoximo!
        return NULL; // não tem proximo retorna o corrente atual mesmo!!!;
    }
    wasInMem = Process_IsInMemory(candidateToCurrentProcess);
    if (!Process_SetMemory(candidateToCurrentProcess, memory, memPolicy)) { // Não tem memória, tenta pegar
        if (swapOut(processList, memory, Process_GetMemoryUsage(candidateToCurrentProcess))) {
            memChanged = true;
            if (!Process_SetMemory(candidateToCurrentProcess, memory, memPolicy)) {
                fprintf(stderr, "\nEstado inválido! Tem coisa errrada!\n");
            }

        } else { // Vamos executar quem já tá na memória mesmo!
            current = getNextReadyProcess(processList, true);
            Process_Start(current);
            return current;

        }
    } else if (!wasInMem) {
        memChanged = true;
    }

    if (current != NULL) {
        if (Process_GetState(current) == Running) {
            Process_Stop(current);
        }
    }
    current = candidateToCurrentProcess;
    if (memChanged) {
        Memory_PrettyPrinter(memory, stdout);
    }
    Process_Start(current);

    return current;
}

void startExec (List processList, MemoryPartition (* memPolicy) (Memory, long))
{

    int     timer    = 0;
    int     smPTimer = 0;
    Memory  memory   = Memory_New();
    Process current  = getNextReadyProcess(processList, false);

    if (!Process_SetMemory(current, memory, memPolicy)) {
        fflush(stdout);
        fflush(stderr);
        fprintf(stderr, "\nSequer temos memoria pro primeiro processo! Algo muito errado.\n");
        return;
    }
    Memory_PrettyPrinter(memory, stdout);
    Process_Start(current); // apenas seta o status


    while (!List_IsEmpty(processList)) { // Enquanto ainda tem processo, continua;
        // chama o timer pra todos processos
        timer++;
        smPTimer++;
        List_DoForAllItems(processList, (void (*) (void*)) Process_Timer);

        if (timer >= maxTime) {
            printf("\n\n Something went wrong!!!\n\n");
            break;
        }


        ProcessState st = Process_GetState(current);
        switch (st) {
            case Finished:
                MemoryPartition_SwapOut(Process_GetMemoryPartition(current));
                if (List_DeleteItem(processList, current)) {
                    current = NULL; // este cara é nulo. Não existe mais!
                }
            case WaitingIO:
                if (current != NULL){
                    printf("\n Processo `Atual` Foi para IO!\t\n");
                }
            case NullPointer:
                if (st == NullPointer){
                    ("\nNullPointer!!\n"); // Não deveria acontecer
                }
            case Ready:
                smPTimer = 0; // ele não tá executando, precisa zerar o tempo!!
                current  = chooseNext(processList, current, memory, memPolicy);
                break;
            case Running:
            default:
                if (smPTimer == 10) { // tempo estourado,passa pro proximo.!
                    smPTimer = 0;
                    Process_PrintLastOperation(current);
                    current  = chooseNext(processList, current, memory, memPolicy);
                    printf("\n\n(t mod 10 == 0) is true (limite de tempo := 10)\t\n\n");
                }
                break;
        }
        //printf("\t(t++)");



    }

    printf("Tempo total: %d", timer);


    Memory_Destroy(memory);


}

void readInput (FILE* input, List processList)
{
    maxTime = 0;
    Process process;
    int     numberOfProcs;
    if (readline(input, _buffer, sizeof(_buffer)) > 0) {
        sscanf(_buffer, "%d", &numberOfProcs);
        // printf("Numero de Processos:%d\n", numberOfProcs);
    } else {
        perror("Não pode ler numero de processos.\n");
        exit(1);
    }

    for (int i = 0; i < numberOfProcs; i++) {
        int procID, memoryUsed, instructionNumber;

        if (readline(input, _buffer, sizeof(_buffer)) <= 0) {
            perror("Não pode ler informações do processo.\n");
            exit(1);
        }

        sscanf(_buffer, "Processo #%d - %dMb", &procID, &memoryUsed);
        //puts(_buffer);
        //printf("Processo #%d - %dMb\n", procID, memoryUsed);

        if (readline(input, _buffer, sizeof(_buffer)) <= 0) {
            perror("Não pode ler numero de instrucoes do processo.\n");
            exit(1);
        }


        sscanf(_buffer, "%d", &instructionNumber);
        // printf("%d\n", instructionNumber);
        process = Process_New(procID, memoryUsed, instructionNumber);


        for (int j = 0; j < instructionNumber; j++) {
            int time;
            readline(input, _buffer, sizeof(_buffer));
            if (sscanf(_buffer, "io %d", &time) == 1) { // É IO
                // printf("io  %d\n", time);
                Process_AddOperation(process, IO, time);
            } else if (sscanf(_buffer, "exec %d", &time) == 1) { // É IO
                //printf("exec  %d\n", time);
                Process_AddOperation(process, CPU, time);
            } else {
                perror("puts!!!!");
                exit(2345);
            }
            maxTime += time;
        }
        List_Append(processList, process);

    }

    printf("\nNumero de processos: %d\n\n", List_NumberOfItems(processList));


}


// from stackoverflow
int readline (FILE* f, char* buffer, size_t len)
{
    char c;
    int  i;

    memset(buffer, 0, len);
    for (i = 0; i < len; i++) {
        int c = fgetc(f);

        if (!feof(f)) {
            if (c == '\r') {
                buffer[i] = 0;
            } else if (c == '\n') {
                buffer[i] = 0;
                return i + 1;
            }
            else {
                buffer[i] = (char) c;
            }
        }
        else {
            //fprintf(stderr, "read_line(): recv returned %d\n", c);
            return -1;
        }
    }

    return -1;
}
