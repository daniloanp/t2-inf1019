//
// Created by danilo on 11/12/15.
//

#include <stdbool.h>

#ifndef T2_INF1019_LIST_H
#define T2_INF1019_LIST_H

typedef struct _list* List;


List List_New (void (* destroy) (void* p));

void List_Destroy (List l);

void List_Append (List l, void* val);

void List_Prepend (List l, void* val);

void List_AddAfterCurrent (List l, void* val);

bool List_AddAfter (List l, void* reference , void* val);

void List_AddBeforeCurrent (List l, void* val);

void List_DoForAllItems (List l, void (* op) (void* p));

void* List_FindByFunc (List l, bool (* finder) (void* p));

bool List_Contains (List l, void* val); // busca por ponteiro

bool List_DeleteCurrent (List l);

bool List_DeleteItem (List l, void* val);

bool List_IsEmpty (List l);

int List_NumberOfItems (List l);

bool List_GoElementWithData(List l, void* reference);

void List_GoFirst (List l);

void List_GoLast (List l);

bool List_GoNext (List l);

bool List_GoPrevious (List l);

void* List_GetCurrent(List l);


#endif //T2_INF1019_LIST_H
