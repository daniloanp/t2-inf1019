//
// Created by danilo on 11/12/15.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "List.h"

typedef struct _listItem* ListItem;
struct _listItem
{
    void* data;
    ListItem next;
    ListItem previous;
};


struct _list
{
    ListItem    currentItem;
    ListItem    firstItem;
    ListItem    lastItem;
    unsigned int numberOfItems;

    void (* destroyItemFunc) (void*);
};


static ListItem NewListItem (void* val)
{
    ListItem i = calloc(1, sizeof(struct _listItem));
    i->data = val;
    return i;
}


List List_New (void (* destroy) (void*))
{
    List l = calloc(1, sizeof(struct _list));
    l->destroyItemFunc = destroy;
    return l;
}

void List_Destroy (List l)
{
    assert(l);

    List_GoFirst(l);
    if (l->destroyItemFunc != NULL) {
        for (; l->currentItem;
               l->currentItem = l->currentItem->next
                ) {

            l->destroyItemFunc(l->currentItem->data);
        }
    }

    free(l);
}

void List_GoFirst (List l)
{
    assert(l);
    if (l->currentItem == NULL) {
        return;
    }
    while (l->currentItem->previous != NULL) {
        l->currentItem = l->currentItem->previous;
    }
}


void List_GoLast (List l)
{
    assert(l);
    if (l->currentItem == NULL) {
        return;
    }
    while (l->currentItem->next != NULL) {
        l->currentItem = l->currentItem->next;
    }
}

bool List_GoNext (List l)
{
    assert(l);
    if (l->currentItem == NULL) {
        return false;
    }
    if (l->currentItem->next == NULL) {
        return false;
    }

    l->currentItem = l->currentItem->next;

    return true;
}


void List_Append (List l, void* val)
{
    assert(l);
    ListItem curr = l->currentItem;
    if (curr != NULL) {
        List_GoLast(l);
        ListItem li = NewListItem(val);
        l->currentItem->next = li;
        li->previous         = l->currentItem;

        l->currentItem       = curr; // Mantem na posição inicial
    } else {
        ListItem li = NewListItem(val);
        l->currentItem = li;
        l->firstItem = li;
        l->lastItem = li;

    }

    l->numberOfItems += 1;
}

void List_Prepend (List l, void* val)
{
    assert(l);
    ListItem curr = l->currentItem;
    if (curr != NULL) {
        List_GoFirst(l);
        ListItem li = NewListItem(val);
        l->currentItem->previous = li;
        li->next                 = l->currentItem;
        l->currentItem           = curr; // Mantem na posição inicial
    } else {
        ListItem li = NewListItem(val);
        l->currentItem = li;
    }
    l->numberOfItems += 1;
}

void List_AddAfterCurrent (List l, void* val)
{
    assert(l);
    ListItem curr = l->currentItem;
    if (curr != NULL) {
        ListItem li        = NewListItem(val);
        ListItem afterItem = l->currentItem->next;
        l->currentItem->next = li;
        li->previous         = l->currentItem;
        if (afterItem != NULL) {
            li->next            = afterItem;
            afterItem->previous = li;
        }
    } else {
        ListItem li = NewListItem(val);
        l->currentItem = li;
    }

    l->numberOfItems += 1;

}


void List_AddBeforeCurrent (List l, void* val)
{
    assert(l);

    if (l->currentItem != NULL) {
        ListItem li         = NewListItem(val);
        ListItem beforeItem = l->currentItem->previous;
        l->currentItem->previous = li;
        li->next                 = l->currentItem;
        if (beforeItem != NULL) {
            li->previous     = beforeItem;
            beforeItem->next = li;
        }
    } else {
        ListItem li = NewListItem(val);
        l->currentItem = li;
    }
    l->numberOfItems += 1;
}

void List_DoForAllItems (List l, void (* op) (void*))
{
    assert(l);
    ListItem li, curr;
    curr = l->currentItem;
    List_GoFirst(l);
    for (li = l->currentItem; li != NULL; li = li->next) {
        op(li->data);
    }
    l->currentItem = curr;
}

void* List_FindByFunc (List l, bool (* finder) (void*))
{
    assert(l);
    ListItem li, curr;
    curr    = l->currentItem;
    for (li = l->currentItem; li != NULL; li = li->next) {
        if (finder(li->data)) {
            return li->data;
        }
    }
    l->currentItem = curr;
    return false;
}

bool List_Contains (List l, void* val)
{
    assert(l);
    ListItem li, curr;
    curr = l->currentItem;
    List_GoFirst(l);
    for (li = l->currentItem; li != NULL; li = li->next) {
        if (li->data == val) {
            return true;
        }
    }
    l->currentItem = curr;
    return false;
}

void static destroyListItem (List l, ListItem li)
{
    if (li->next != NULL && li->previous != NULL) { // tem ambos vizinhos
        li->previous->next = li->next;
        li->next->previous = li->previous;
        l->currentItem     = li->next;
    } else if (li->next != NULL) { // só proximo um
        li->next->previous = NULL; // o anterior do meu proximo é numo;
        l->currentItem     = li->next;
    } else if (li->previous != NULL) {
        li->previous->next = NULL;
        l->currentItem     = li->previous;
    } else {
        l->currentItem = NULL;
    }

    l->numberOfItems--;

    l->destroyItemFunc(li->data);
    free(li);
}

bool List_DeleteCurrent (List l)
{
    assert(l);
    if (l->currentItem) {
        ListItem curr = l->currentItem;
        destroyListItem(l, curr);
        return true;
    }
    return false;
}

bool List_DeleteItem (List l, void* val)
{
    assert(l);
    ListItem li = NULL, curr;
    bool     found = false;
    bool isCurrent = false;

    curr = l->currentItem;
    if (val == NULL) {
        return false;
    }
    if (curr->data == val) {
        li    = curr;
        found = true;
        isCurrent = true;
    } else {
        List_GoFirst(l);
        for (li = l->currentItem; li != NULL; li = li->next) {
            if (li->data == val) {
                found = true;
                break;
            }
        }
    }

    if (!found) {
        return false;
    }

    destroyListItem(l, li);

    if (!isCurrent) {
        l->currentItem = curr;
    }



    return true;


}

bool List_IsEmpty (List l)
{
    assert((l->currentItem == NULL && l->numberOfItems == 0) || (l->currentItem != NULL && l->numberOfItems > 0));
    return l->currentItem == NULL;
}

bool List_GoPrevious (List l)
{

    assert(l);
    if (l->currentItem && l->currentItem->previous) {
        l->currentItem = l->currentItem->previous;
        return true;
    }
    return false;


    return false;
}

void* List_GetCurrent (List l)
{
    assert(l);
    if (l->currentItem == NULL) {
        return NULL;
    }
    return l->currentItem->data;
}

int List_NumberOfItems (List l)
{
    return l->numberOfItems;
}


bool List_GoElementWithData(List l, void* reference) {
    List_GoFirst(l);
    ListItem li;
    for (li = l->currentItem; li != NULL; li = li->next) {
        if (li->data == reference) {
            return true;
        }
    }
    return false;
}
bool List_AddAfter (List l, void* reference, void* val)
{
    assert(l);
    ListItem li, curr;
    curr = l->currentItem;


    l->currentItem = curr;

}
