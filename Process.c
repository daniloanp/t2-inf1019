//
// Created by danilo on 15/11/15.
//


#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "Process.h"

typedef struct _processOperation* Operation;
struct _processOperation
{
    ProcessOperation op;
    int              totalTime;
    int              timeElapsed;
};

struct _process
{
    int id;
    int memoryUsage;
    int numberOfOperations;
    int currentOperationIndex;
    int stoppedTime;

    int totalStoppedTime;
    int totalTimeInIO;
    int totalTimeInCPU;

    ProcessState state;
    Operation* operations;
    MemoryPartition memoryPartition;
    long            initMemoryOffSet;
};

static Operation Instruction_New (ProcessOperation op, int totalTime)
{
    Operation i = calloc(1, sizeof(struct _processOperation));
    i->op          = op;
    i->totalTime   = totalTime;
    i->timeElapsed = 0;
    return i;
}

Process Process_New (int id, int memUsage, int numberOfOperations)
{
    Process p = calloc(1, sizeof(struct _process));
    p->id                 = id;
    p->memoryUsage        = memUsage;
    p->numberOfOperations = numberOfOperations;
    p->operations         = calloc((size_t) numberOfOperations, sizeof(Operation));
    p->state              = Ready;
    p->memoryPartition    = NULL;
    return p;
}

ProcessState Process_GetState (Process process)
{
    if (process == NULL) {
        return NullPointer;
    }
    return process->state;
}

void Process_AddOperation (Process process, ProcessOperation op, int time)
{
    Operation i = Instruction_New(op, time);
    for (int  j = 0; j < process->numberOfOperations; j++) {
        if (process->operations[j] == NULL) {
            process->operations[j] = i;
            return;
        }
    }
    perror("Você excedeu os slots de operação");
    exit(1);

}

void Process_Destroy (Process process)
{
    if (process->memoryPartition != NULL) {
        Process_SetMemoryPartition(process, NULL);
    }
    for (int j = 0; j < process->numberOfOperations; j++) {
        free(process->operations[j]);
    }

    free(process->operations);
    free(process);
}

static Operation Process_GetCurrentOperation (Process process)
{
    assert(process->currentOperationIndex < process->numberOfOperations);
    return process->operations[process->currentOperationIndex];
}

static Operation Process_GoNextOperation (Process process)
{
    process->currentOperationIndex++;
    if (process->currentOperationIndex >= process->numberOfOperations) {
        return NULL;
    }
    return Process_GetCurrentOperation(process);
}


void Process_PrintExecInfo (Process process)
{
    printf("\n Tempo total: %d", process->totalTimeInCPU + process->totalStoppedTime + process->totalTimeInIO);
    printf(" [Tempo parado: %d, Tempo de IO: %d, Tempo de CPU:%d ]\n", process->totalStoppedTime,
           process->totalTimeInIO, process->totalTimeInCPU);
}

void Process_Timer (Process process)
{
    // printf("\n===>\t Timer para processo [%d] status %d, %d\n", process->id, process->state, Process_IsInMemory(process));

    if (process->state == Finished) {
        assert(0);
        return;
    }
    if (process->state == Ready) {
        process->stoppedTime += 1;
        process->totalStoppedTime += 1;
        return;
    }

    Operation currentOp = Process_GetCurrentOperation(process);
    currentOp->timeElapsed++; // eee, andou

    if (currentOp->totalTime == currentOp->timeElapsed) { // eee terminou um passo
        if (currentOp->op == CPU) {
            process->totalTimeInCPU += currentOp->totalTime;
            printf("END:\t [%d][%d] `exec %d` Terminou!\n", process->id, process->currentOperationIndex,
                   currentOp->totalTime);
        } else {
            process->totalTimeInIO += currentOp->totalTime;
            printf("END:\t [%d][%d] `io %d` Terminou!\n", process->id, process->currentOperationIndex,
                   currentOp->totalTime);
        }

        // Agora começa a lógica (código acima é pra debugging);
        Operation nextOp = Process_GoNextOperation(process);
        if (nextOp == NULL) { // o processo terminou!!!
            process->state       = Finished;
            process->stoppedTime = 0;
            Process_PrintExecInfo(process);
            printf(" Processo [%d] Terminou!!\n\n", process->id);

            return;
        } else { // Tem proxima operação
            if (nextOp->op == CPU) { // Ok, é pra ficar quieto esperando.
                process->state = Ready;
            } else if (nextOp->op == IO) {
                process->state = WaitingIO; // em IO;
            }
        }

    } else {
//        if (currentOp->op == CPU) {
//            printf("\t\t[%d][%d] `exec %d` passou %d!\n", process->id, process->currentOperationIndex, currentOp->totalTime, currentOp->timeElapsed);
//        } else {
//            printf("\t\t[%d][%d] `io %d` passou %d !\n", process->id, process->currentOperationIndex, currentOp->totalTime, currentOp->timeElapsed);
//        }
    }

}

bool Process_Start (Process p)
{
    ProcessState state = Process_GetState(p);
    if (state == NullPointer) {
        return false;
    }
    if (p->state == Ready) {
        printf("->Processo [%d] iniciado!\n", p->id);
    }
    if (p->state == Finished) {
        return false;
    }
    int inx        = p->currentOperationIndex;
    if (p->operations[inx]->op == CPU) {
        p->state = Running;
    } else {
        p->state = WaitingIO;
    }
    p->stoppedTime = 0;

}

void Process_Stop (Process p)
{
    printf("->Proecsso [%d] pausado!\n", p->id);
    assert(Process_GetState(p) == Running);
    p->stoppedTime = 0;
    p->state       = Ready;
}

int Process_GetTimeStopped (Process p)
{
    return p->stoppedTime;
}

bool Process_IsInMemory (Process p)
{
    return (p->memoryPartition != NULL);
}

bool Process_SetMemory (Process process, Memory memory, MemoryPartition ( * memPolicy) (Memory, long))
{
    if (Process_IsInMemory(process)) {
        return true;
    }
    MemoryPartition partition = memPolicy(memory, process->memoryUsage);
    if (partition == NULL) {
        return false;
    }
    printf("\n->Processo [ %d ] alocado (usa: %d MB)\n", process->id, process->memoryUsage);
    //MemoryInfo_SetProcessID(memoryPartition, process->id);
    MemoryPartition_SetProcessID(partition, process->id);
    Process_SetMemoryPartition(process, partition);

    return true;

}

void Process_SetMemoryPartition (Process p, MemoryPartition partition)
{
    if (partition == NULL && p->memoryPartition != NULL) {
        MemoryPartition_SwapOut(p->memoryPartition);
    }
    p->memoryPartition = partition;
}

MemoryPartition Process_GetMemoryPartition (Process process)
{
    return process->memoryPartition;
}

long Process_GetMemoryUsage (Process processs)
{
    return processs->memoryUsage;
}

int Process_GetID (Process process)
{
    return process->id;
}

void Process_PrintLastOperation (Process process)
{

    Operation operation = Process_GetCurrentOperation(process);
    if (operation != NULL) {
        if (operation->op == CPU) {
            printf("\n\t\t[%d][%d] `exec %d` passou %d!\n", process->id, process->currentOperationIndex,
                   operation->totalTime, operation->timeElapsed);
        } else {
            printf("\n\t\t[%d][%d] `io %d` passou %d !\n", process->id, process->currentOperationIndex,
                   operation->totalTime, operation->timeElapsed);
        }
    }
}
