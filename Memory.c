//
// Created by danilo on 15/11/15.
//


#include <stdlib.h>
#include <limits.h>
#include <assert.h>
#include "Memory.h"


#define MEM_NUM_PART  5

//struct _memInfo
//{
//    bool isFree;
//    long initOffSet;
//    long endOffSet;
//    long length;
//    int  partitionIndex;
//    int  processID; // se for -1 é vazio!
//};


struct _partition
{
    int  myIndex;
    int  processID;
    long totalLength;
    long usedLength;
    List slices; // Lista de memInfo
};

struct _mem
{
    MemoryPartition partitions[MEM_NUM_PART];
    int             lastUsedPartitionIndex;
//    MemoryInfo      lastSlice;

};

static long partitionsSize[MEM_NUM_PART] = {
        8,
        4,
        2,
        1,
        1
};

//bool Memory_Contains (Memory memory, MemoryInfo memoryInfo)
//{
//    for (int i = 0; i < MEM_NUM_PART; i++) {
//        if (memory->partitions[i]) {
//            if (List_Contains(memory->partitions[i]->slices, memoryInfo)) {
//                return true;
//            }
//        }
//    }
//    return false;
//}

//static MemoryInfo MemoryInfo_New (int partition, long init, long length, bool isFree)
//{
//    MemoryInfo memoryPartition = (MemoryInfo) calloc(1, sizeof(struct _memInfo));
//    memoryPartition->partitionIndex = partition;
//    memoryPartition->isFree         = isFree;
//    memoryPartition->initOffSet     = init;
//    memoryPartition->endOffSet      = (init + length) - 1;
//    memoryPartition->length         = length;
//    memoryPartition->processID      = - 1;
//    return memoryPartition;
//}

static MemoryPartition Partition_New (int partitionIndex, long size)
{
    MemoryPartition partition = (MemoryPartition) calloc(1, sizeof(struct _partition));
    partition->myIndex     = partitionIndex;
    partition->totalLength = size;
    partition->processID   = -1;
    return partition;
}

//static long Partition_FreeMemory (MemoryPartition partition)
//{
//    long freeMem = partition->size - partition->used;
//    assert(freeMem >= 0);
//    return freeMem;
//}

static const long mb = 1; // mb is the minimum size;


Memory Memory_New (void)
{

    Memory   mem = calloc(1, sizeof(struct _mem));
    for (int i   = 0; i < MEM_NUM_PART; i++) {
        mem->partitions[i] = Partition_New(i, partitionsSize[i]);
    }

    return mem;
}


void Memory_Destroy (Memory memory)
{

    for (int i = 0; i < MEM_NUM_PART; i++) {
        //List_Destroy(memory->partitions[i]->slices);
        free(memory->partitions[i]);
    }

    free(memory);

}


//static MemoryInfo fixingFragments (MemoryInfo slot, List list, long requiredMemory)
//{
//    if (slot == NULL) {
//        return slot;
//    }
//    if (slot->length > requiredMemory) {
//        MemoryInfo usedSlot = MemoryInfo_New(slot->partitionIndex, slot->initOffSet, requiredMemory, false);
//        MemoryInfo freeSlot = MemoryInfo_New(slot->partitionIndex, usedSlot->endOffSet + 1,
//                                             (slot->length - requiredMemory), true);
//        if (!List_GoElementWithData(list, slot)) {
//            assert(0);
//        };
//
//        List_AddAfterCurrent(list, usedSlot);
//        List_GoNext(list);
//        List_AddAfterCurrent(list, freeSlot);
//        List_DeleteItem(list, slot);
//        slot = usedSlot;
//    } else {
//        slot->isFree = false;
//    }
//
//    return slot;
//}

bool MemoryPartitionIsEmpty (MemoryPartition partition)
{
    return partition->usedLength == 0;
}

MemoryPartition Memory_SwapIn_BestFit (Memory memory, long requiredMemory)
{

    long            minFreeMem = LONG_MAX;
    MemoryPartition partition  = NULL;

    for (int i = 0; i < MEM_NUM_PART; i++) { // para cada partição;
        MemoryPartition pt = memory->partitions[i];
        if (!MemoryPartitionIsEmpty(pt)) {
            continue; // usada!
        }
        long elapsedMem = pt->totalLength - requiredMemory;
        if (elapsedMem < 0) { // não pode ser menor, se não não tem espaço
            continue;
        }
        if (elapsedMem <0 && requiredMemory == 8) {
            printf("sdfahfjgdsbhjfbdshdjfgsd");
        }
        if (elapsedMem < minFreeMem) { // a menor possível
            minFreeMem = elapsedMem;
            partition  = memory->partitions[i];
        }
    }
    if (partition != NULL) {
        partition->usedLength = requiredMemory;
    }
    return partition;
}


MemoryPartition Memory_SwapIn_FirstFit (Memory memory, long requiredMemory)
{
    MemoryPartition partition = NULL;

    for (int i = 0; i < MEM_NUM_PART; i++) { // para cada partição;
//        List l = memory->partitions[i]->slices;
//        List_GoFirst(l);
//        do {
//            MemoryInfo mi = List_GetCurrent(l);
//            if (mi->isFree) {
//                long elapsedMem = mi->length - requiredMemory;
//                if (elapsedMem < 0) { // não pode ser menor, se não não tem espaço
//                    continue;
//                }
//                slot = mi;
//                list = l;
//                break;
//            }
//            if (slot != NULL) { // já achou alguém que dá, acabou!;
//                break;
//            }
//        } while (List_GoNext(l));
    }

    for (int i = 0; i < MEM_NUM_PART; i++) { // para cada partição;
        MemoryPartition pt = memory->partitions[i];
        if (!MemoryPartitionIsEmpty(pt)) {
            continue; // usada!
        }
        long elapsedMem = pt->totalLength - requiredMemory;
        if (elapsedMem < 0) { // não pode ser menor, se não não tem espaço
            continue;
        }
        partition = pt; // coube para!;
        break;

    }
    if (partition != NULL) {
        partition->usedLength = requiredMemory;
    }
    return partition;
}


MemoryPartition Memory_SwapIn_WorstFit (Memory memory, long requiredMemory)
{
    long            maxFreeMem = -1;
    MemoryPartition partition  = NULL;
//    for (int   i          = 0; i < MEM_NUM_PART; i++) { // para cada partição;
//        List l = memory->partitions[i]->slices;
//        List_GoFirst(l);
//        do {
//            MemoryInfo mi = List_GetCurrent(l);
//            if (mi->isFree) {
//                long elapsedMem = mi->length - requiredMemory;
//                if (elapsedMem < 0) { // não pode ser menor, se não não tem espaço
//                    continue;
//                }
//                if (elapsedMem > maxFreeMem) { // a maior possível
//                    maxFreeMem = elapsedMem; // anota a maior
//                    slot       = mi;
//                    list       = l;
//                }
//            }
//        } while (List_GoNext(l));
//    }

    for (int i = 0; i < MEM_NUM_PART; i++) { // para cada partição;
        MemoryPartition pt = memory->partitions[i];
        if (!MemoryPartitionIsEmpty(pt)) {
            continue; // usada!
        }
        long elapsedMem = pt->totalLength - requiredMemory;
        if (elapsedMem < 0) { // não pode ser menor, se não não tem espaço
            continue;
        }
        if (elapsedMem > maxFreeMem) { // a maior possível
            maxFreeMem = elapsedMem; // anota a maior
            partition = pt;
        }
    }

    if (partition != NULL) {
        partition->usedLength = requiredMemory;
    }
    return partition;


}

MemoryPartition Memory_SwapIn_NextFit (Memory memory, long requiredMemory)
{
    MemoryPartition partition  = NULL;
    for (int i = memory->lastUsedPartitionIndex; i < MEM_NUM_PART; i++) { // para cada partição;
        MemoryPartition pt = memory->partitions[i];
        if (!MemoryPartitionIsEmpty(pt)) {
            continue; // usada!
        }
        long elapsedMem = pt->totalLength - requiredMemory;
        if (elapsedMem < 0) { // não pode ser menor, se não não tem espaço
            continue;
        }
        memory->lastUsedPartitionIndex = i;
        partition = pt; // coube para!;
        break;
    }

    if (partition == NULL) {
        for (int i = 0; i < memory->lastUsedPartitionIndex; i++) { // para cada partição;
            MemoryPartition pt = memory->partitions[i];
            if (!MemoryPartitionIsEmpty(pt)) {
                continue; // usada!
            }
            long elapsedMem = pt->totalLength - requiredMemory;
            if (elapsedMem < 0) { // não pode ser menor, se não não tem espaço
                continue;
            }
            memory->lastUsedPartitionIndex = i;
            partition = pt; // coube para!;
            break;
        }
    }

    if (partition != NULL) {
        partition->usedLength = requiredMemory;
    }
    return partition;
//    MemoryInfo slot    = NULL;
//    List       list    = NULL;
//    int        inxUsed = -1;
//    int        i;
//    if (memory->lastSlice != NULL) {
//        i = memory->lastSlice->partitionIndex;
//    } else {
//        i = 0;
//    }
//    for (i; i < MEM_NUM_PART; i++) { // para cada partição a partir da ultima
//        List l = memory->partitions[i]->slices;
//
//        // Estamos no "primeiro indice E"
//        if (i == memory->lastUsedPartitionIndex && memory->lastSlice != NULL) {
//            List_GoElementWithData(l, memory->lastSlice);
//            if (!List_GoNext(l)) {
//                continue;
//            }
//        } else {
//            List_GoFirst(l);
//        }
//
//        do {
//            MemoryInfo mi = List_GetCurrent(l);
//            if (mi->isFree) {
//                long elapsedMem = mi->length - requiredMemory;
//                if (elapsedMem < 0) { // não pode ser menor, se não não tem espaço
//                    continue;
//                }
//                slot = mi;
//                break;
//            }
//        } while (List_GoNext(l));
//        if (slot != NULL) { // já achou alguém que dá, acabou!;
//            list    = l;
//            inxUsed = i;
//            break;
//        }
//    }
//
//    if (slot == NULL && memory->lastSlice != NULL) { // vamos procurar do inicio
//        for (i = 0; i <= memory->lastUsedPartitionIndex; i++) { // para cada partição;
//            List l = memory->partitions[i]->slices;
//            List_GoFirst(l);
//            do {
//                MemoryInfo mi = List_GetCurrent(l);
//                if (mi == memory->lastSlice) {//atingiu o último usado;
//                    break;
//                }
//                if (mi->isFree) {
//                    long elapsedMem = mi->length - requiredMemory;
//                    if (elapsedMem < 0) { // não pode ser menor, se não não tem espaço
//                        continue;
//                    }
//                    slot = mi;
//                    break;
//                }
//            } while (List_GoNext(l));
//            if (slot != NULL) { // já achou alguém que dá, acabou!;
//                list    = l;
//                inxUsed = i;
//                break;
//            }
//        }
//    }
//
//
//    slot = fixingFragments(slot, list, requiredMemory);
//    if (slot != NULL) {
//        memory->lastSlice = slot;
//    }
//
//
//    return slot;
}



void MemoryPartition_SetProcessID (MemoryPartition partition, int processID)
{
    partition->processID = processID;

}

//void MemoryInfo_SwapOut (Memory memory, MemoryInfo memoryPartition)
//{
//    assert(memoryPartition != NULL);
//    List partitionSlices = memory->partitions[memoryPartition->partitionIndex]->slices;
//    assert(List_GoElementWithData(partitionSlices, memoryPartition)); // se der merda aqui, tem coisa errada
//    int counter = 0;
//    while (List_GoNext(partitionSlices)) {
//        MemoryInfo mi = List_GetCurrent(partitionSlices);
//        if (mi->isFree) {
//            counter++;
//        } else {
//            List_GoPrevious(partitionSlices);
//            break;
//        }
//    }
//    if (counter == 0) {
//        memoryPartition->isFree = true;
//        return;
//    }
//
//    long endOffSet;
//    long beginOffSet;
//
//    MemoryInfo mi = List_GetCurrent(partitionSlices);
//    endOffSet = mi->endOffSet;
//    do {
//        MemoryInfo mi_ = List_GetCurrent(partitionSlices);
//        if (!List_GoPrevious(partitionSlices)) { // não deu pra voltar
//            break;
//        }
//        List_DeleteItem(partitionSlices, mi_);
//        mi             = List_GetCurrent(partitionSlices);
//    } while (mi != memoryPartition);
//
//    beginOffSet = mi->initOffSet;
//    List_GoElementWithData(partitionSlices, mi);
//    List_AddAfterCurrent(partitionSlices,
//                         MemoryInfo_New(mi->partitionIndex, beginOffSet, (endOffSet - beginOffSet) + 1, true));
//    List_DeleteItem(partitionSlices, mi);
//}
void MemoryPartition_SwapOut (MemoryPartition partition)
{
    partition->processID = -1;
    partition->usedLength = 0;
}

//static void slicePrettyPrint (List slice)
//{
//    assert(!List_IsEmpty(slice));
//
//    MemoryInfo info;
//    List_GoFirst(slice);
//    do {
//        info = List_GetCurrent(slice);
//        for (int i = 0; i < info->length; i++) {
//            if (info->isFree) {
//                printf("[ ___ ]");
//            } else {
//                printf("[ %3d ]", info->processID);
//            }
//        }
//    } while (List_GoNext(slice));
//}

void Memory_PrettyPrinter (Memory memory, FILE* file)
{
    printf("\n\n=:= Estado da Memória =:=\n");
    for (int i = 0; i < MEM_NUM_PART; i++) {
        MemoryPartition partition = memory->partitions[i];
        printf("\t\t");
        for (int j = 0; j < partition->totalLength; j++) {

            if (partition->processID == -1 || j+1 > partition->usedLength) {
                printf("[ ___ ]");
            } else {
                printf("[ %3d ]", partition->processID);
            }
        }
        printf("\n");
    }
    printf("\n");
}

#undef MEM_NUM_PART


long MemoryPartition_GetLength (MemoryPartition memoryPartition)
{
    return memoryPartition->totalLength;
}
